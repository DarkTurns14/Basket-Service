FROM node:20-alpine

RUN apk add --no-cache git

RUN git clone https://gitlab.com/DarkTurns14/Basket-Service-Helper.git

WORKDIR /Basket-Service-Helper

RUN npm install

CMD npm start
